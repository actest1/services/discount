<?php

namespace App\Traits;

use App\Enum\DiscountType;
use App\Models\DiscountCode;
use App\Models\DiscountCodeArchive;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait DiscountTrait
{
    public function archive(DiscountCode $discount): DiscountCodeArchive
    {
        $archived_discount = $discount->toArray();

        unset(
            $archived_discount['created_at'], $archived_discount['created_by'], $archived_discount['updated_at'],
            $archived_discount['updated_by'], $archived_discount['deleted_at'], $archived_discount['deleted_by'],
            $archived_discount['id']
        );

        return $discount
            ->archives()
            ->create($archived_discount);
    }

    public function generateDiscountCode(int $count = 1, int $length = 10, ?string $prefix = null): Collection
    {
        $codes = collect();

        for ($i = 0; $i < $count; $i++) {
            while($code = str($prefix . Str::random($length))->lower()) {
                if (DiscountCode::query()->where('code', $code)->doesntExist() && !$codes->search($code))
                    break;
            }

            $codes->push($code);
        }

        return $codes;
    }

    public function updateDiscount(DiscountCode $discount, array $attributes): bool
    {
        $type = DiscountType::tryFrom($attributes['type'] ?? false);

        if ($type == DiscountType::STATIC)
            $attributes['max_amount'] = null;

        return DB::transaction(function () use($discount, $attributes) {
            $this->archive($discount);

            return $discount->update($attributes);
        });
    }
}
