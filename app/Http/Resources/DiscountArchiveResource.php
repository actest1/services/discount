<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscountArchiveResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'discount' => new DiscountResource($this->discountCode),
            'status' => $this->status,
            'description' => $this->description,
            'code' => $this->code,
            'currency_id' => $this->currency_id,
            'type' => $this->type,
            'amount' => $this->amount,
            'max_amount' => $this->max_amount,
            'expire_date' => $this->expire_date?->format('Y-m-d H:i:s'),
            'submitted_times' => $this->submitted_times,
            'max_times' => $this->max_times,
            'per_use_once' => $this->per_user_once,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'created_by' => $this->created_by,
        ];
    }
}
