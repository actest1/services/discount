<?php

namespace App\Http\Requests;

use App\Enum\DiscountCodeStatus;
use App\Enum\DiscountType;
use App\Rules\DescriptionRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class DiscountGenerateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $is_static = DiscountType::tryFrom($this->request->get('type')) == DiscountType::STATIC;

        return [
            'status' => [
                'required',
                new Enum(DiscountCodeStatus::class)
            ],
            'description' => [
                new DescriptionRule(),
                'max:255'
            ],
            'prefix' => ['alpha_dash', 'max:10'],
            'count' => ['required', 'int', 'between:1,5000'],
            'currency_id' => ['required', 'integer'],
            'type' => [
                'required',
                new Enum(DiscountType::class)
            ],
            'amount' => ['required', 'numeric'],
            'max_amount' => [
                Rule::excludeIf($is_static),
                'nullable',
                'numeric'
            ],
            'expire_date' => ['nullable', 'date_format:Y-m-d H:i:s'],
            'max_times' => ['nullable', 'integer'],
            'per_user_once' => ['boolean']
        ];
    }
}
