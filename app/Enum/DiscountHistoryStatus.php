<?php
namespace App\Enum;

enum DiscountHistoryStatus: int {
    case SUCCESS = 1;
    case EXPIRED = 2;
    case FAIL = 3;
}
