<?php
namespace App\Enum;

enum DiscountCodeStatus: int {
    case DISABLE = 0;
    case ACTIVE = 1;

    // Temporary state
    case SUSPENSION = 2;
}
