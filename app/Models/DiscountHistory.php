<?php

namespace App\Models;

use App\Enum\DiscountHistoryStatus;
use App\Enum\DiscountType;
use App\Traits\CreatedAt;
use App\Traits\CreatedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountHistory extends Model
{
    use HasFactory, CreatedAt, CreatedBy;
    public $timestamps = false;

    protected $casts = [
        'status' => DiscountHistoryStatus::class,
        'submitted_discount_type' => DiscountType::class,
        'created_at' => 'datetime'
    ];

    public function scopeFilter(Builder $query, array $filters)
    {
        $query->when($filters['status'] ?? false, fn($query, $status) =>
            $query->where('status', $status)
        );

        $query->when($filters['discount_id'] ?? false, fn($query, $discount_id) =>
            $query->where('discount_id', $discount_id)
        );

        $query->when($filters['order_id'] ?? false, fn($query, $order_id) =>
            $query->where('order_id', $order_id)
        );

        $query->when($filters['user_id'] ?? false, fn($query, $user_id) =>
            $query->where('user_id', $user_id)
        );

        $query->when($filters['type'] ?? false, fn($query, $type) =>
            $query->where('type', $type)
        );

        $query->when($filters['created_by'] ?? false, fn($query, $created_by) =>
            $query->where('created_by', $created_by)
        );

    }

    public function discountCode()
    {
        return $this->belongsTo(DiscountCode::class, 'discount_id');
    }
}
