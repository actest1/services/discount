<?php

namespace App\Enum;

enum DiscountType: int
{
    case STATIC = 0;
    case DYNAMIC = 1;
}
