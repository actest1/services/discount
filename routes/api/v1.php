<?php

use App\Http\Controllers\v1\DiscountArchiveController;
use App\Http\Controllers\v1\DiscountController;
use App\Http\Controllers\v1\DiscountHistoryController;
use Illuminate\Support\Facades\Route;

Route::resource('discount', DiscountController::class)
    ->except(['create', 'edit']);

Route::post('discount/generate', [DiscountController::class, 'generate']);
Route::post('discount/redeem', [DiscountController::class, 'redeem']);

Route::get('/discount-archive', [DiscountArchiveController::class, 'index']);
Route::get('/discount-archive/{archive}', [DiscountArchiveController::class, 'show']);

Route::get('/discount-history', [DiscountHistoryController::class, 'index']);
Route::get('/discount-history/{history}', [DiscountHistoryController::class, 'show']);
