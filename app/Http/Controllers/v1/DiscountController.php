<?php

namespace App\Http\Controllers\v1;

use App\Enum\SystemMessage;
use App\Exceptions\Discount\RedeemException;
use App\Http\Controllers\Controller;
use App\Http\Requests\DiscountGenerateRequest;
use App\Http\Requests\DiscountRedeemRequest;
use App\Http\Requests\DiscountStoreRequest;
use App\Http\Requests\DiscountUpdateRequest;
use App\Http\Resources\DiscountCollection;
use App\Http\Resources\DiscountHistoryResource;
use App\Http\Resources\DiscountResource;
use App\Models\DiscountCode;
use App\Services\v1\DiscountService;
use App\Traits\DiscountTrait;
use Illuminate\Database\QueryException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\Response as HttpFoundation;

class DiscountController extends Controller
{
    use DiscountTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = request([
            'status',
            'code',
            'currency_id',
            'type',
            'created_by',
            'updated_by'
        ]);

        $discounts = DiscountCode::query()
            ->filter($filters)
            ->orderByDesc('id')
            ->paginate(10);

        return Response::success(
            SystemMessage::SUCCESS, 'Data fetched',
            new DiscountCollection($discounts)
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param DiscountStoreRequest $discountStoreRequest
     * @return \Illuminate\Http\Response
     */
    public function store(DiscountStoreRequest $discountStoreRequest)
    {
        $attributes = $discountStoreRequest->validated();

        $discount = DiscountCode::query()
            ->create($attributes);

        return Response::success(
            SystemMessage::SUCCESS, 'Discount created successfully',
            new DiscountResource($discount), HttpFoundation::HTTP_CREATED
        );
    }

    /**
     * Display the specified resource.
     *
     * @param DiscountCode $discount
     * @return \Illuminate\Http\Response
     */
    public function show(DiscountCode $discount)
    {
        return Response::success(
            SystemMessage::SUCCESS, 'Data fetched',
            new DiscountResource($discount)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DiscountCode $discount
     * @param DiscountUpdateRequest $discountUpdateRequest
     * @return \Illuminate\Http\Response
     */
    public function update(DiscountCode $discount, DiscountUpdateRequest $discountUpdateRequest)
    {
        $attributes = $discountUpdateRequest->validated();

        try {
           $this->updateDiscount($discount, $attributes);

            return Response::success(SystemMessage::SUCCESS, 'Discount updated successfully', new DiscountResource($discount));
        } catch (QueryException $exception) {

            return Response::error(
                SystemMessage::INTERNAL_ERROR, 'Database transaction has failed',
                null, HttpFoundation::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DiscountCode $discount
     * @return \Illuminate\Http\Response
     */
    public function destroy(DiscountCode $discount)
    {
        $discount->delete();

        return Response::success(SystemMessage::SUCCESS, 'Discount deleted successfully');
    }


    public function generate(DiscountGenerateRequest $discountGenerateRequest)
    {
        $attributes = $discountGenerateRequest->validated();

        $codes = $this->generateDiscountCode($attributes['count'], 18, $attributes['prefix']);

        unset($attributes['count'], $attributes['prefix']);

        $date = Carbon::now();

        $bulk_data = $codes
            ->map(fn($code) => array_merge([
                'code' => $code,
                'created_by' => auth()->user()->id,
                'created_at' => $date
            ], $attributes));

        DiscountCode::insert($bulk_data->toArray());

        return Response::success(
            SystemMessage::SUCCESS, 'Discount code generated',
            [
                'codes' => $codes
            ]
        );
    }

    public function redeem(DiscountService $discountService, DiscountRedeemRequest $discountRedeemRequest)
    {
        $attributes = $discountRedeemRequest->validated();

        try {
            $discountHistory = $discountService->redeem($attributes['order_id'], $attributes['user_id'], $attributes['currency_id'], $attributes['code']);

            return Response::success(
                SystemMessage::SUCCESS, 'Discount has been redeemed',
                new DiscountHistoryResource($discountHistory)
            );
        } catch(RedeemException $exception) {
            return Response::error(
                $exception->getCode(), $exception->getMessage(),
                []
            );
        }
    }
}
