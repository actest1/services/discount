<?php

namespace App\Traits;

trait UpdatedBy
{
    public static function bootUpdatedBy()
    {
        static::updating(function ($model) {
            if ($model->isClean('updated_by') && auth()->hasUser())
                $model->updated_by = auth()->user()->id;
        });
    }

}
