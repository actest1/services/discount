<?php

namespace App\Traits;

trait CreatedBy
{
    public static function bootCreatedBy()
    {
        static::creating(function ($model) {
            if ($model->isClean('created_by') && auth()->hasUser())
                $model->created_by = auth()->user()->id;
        });
    }

}
