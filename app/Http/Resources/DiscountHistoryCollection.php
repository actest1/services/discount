<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DiscountHistoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'histories' => DiscountHistoryResource::collection($this->collection),
            'meta' => [
                'count' => $this->count(),
                'current_page' => $this->currentPage(),
                'last_page' => $this->lastPage()
            ]
        ];
    }
}
