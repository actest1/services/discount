<?php

namespace App\Http\Controllers\v1;

use App\Enum\SystemMessage;
use App\Http\Controllers\Controller;
use App\Http\Resources\DiscountHistoryCollection;
use App\Http\Resources\DiscountHistoryResource;
use App\Models\DiscountHistory;
use Illuminate\Support\Facades\Response;

class DiscountHistoryController extends Controller
{
    public function index()
    {
        $filters = request([
            'status',
            'discount_id',
            'order_id',
            'user_id',
            'type',
            'created_by'
        ]);

        $histories = DiscountHistory::query()
            ->filter($filters)
            ->with('discountCode')
            ->orderByDesc('id')
            ->paginate(10);

        return Response::success(
            SystemMessage::SUCCESS, 'Data fetched',
            new DiscountHistoryCollection($histories)
        );
    }

    public function show(DiscountHistory $history)
    {
        return Response::success(
            SystemMessage::SUCCESS, 'Data fetched',
            new DiscountHistoryResource($history)
        );
    }
}

