<?php

namespace App\Exceptions;

use App\Enum\SystemMessage;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Symfony\Component\HttpFoundation\Response as HttpFoundation;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function(NotFoundHttpException $exception, Request $request) {
            return Response::error(
                SystemMessage::DATA_NOT_FOUND, 'Data not found',
                [], HttpFoundation::HTTP_NOT_FOUND
            );
        });

        $this->renderable(function(ValidationException $exception, Request $request) {
            return Response::error(
                SystemMessage::BAD_DATA, 'Validation failed',
                $exception->errors(), HttpFoundation::HTTP_BAD_REQUEST
            );
        });


        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
