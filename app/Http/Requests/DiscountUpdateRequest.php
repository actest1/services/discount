<?php

namespace App\Http\Requests;

use App\Enum\DiscountCodeStatus;
use App\Enum\DiscountType;
use App\Rules\DescriptionRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class DiscountUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $is_static = $this->request->get('type') == DiscountType::STATIC;

        return [
            'status' => [
                new Enum(DiscountCodeStatus::class)
            ],
            'description' => [
                'nullable',
                new DescriptionRule(),
                'max:255'
            ],
            'code' => [
                'alpha_dash',
                'max:30',
                Rule::unique('discount_codes', 'code')->ignore($this->route('discount')->id)
            ],
            'currency_id' => ['integer'],
            'type' => [
                new Enum(DiscountType::class)
            ],
            'amount' => ['numeric'],
            'max_amount' => [
                Rule::excludeIf($is_static),
                'nullable',
                'numeric'
            ],
            'expire_date' => ['nullable', 'date_format:Y-m-d H:i:s'],
            'max_times' => ['nullable', 'integer'],
            'per_user_once' => ['boolean']
        ];
    }
}
