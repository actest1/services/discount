<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('status');
            $table
                ->foreignId('discount_id')
                ->references('id')
                ->on('discount_codes')
                ->restrictOnDelete();

            $table->unsignedInteger('order_id');
            $table->unsignedInteger('user_id');
            $table->unsignedFloat('type');

            $table->unsignedDecimal('amount', 19, 4)
                ->comment('Discount amount');

            $table->unsignedDecimal('max_amount', 19, 4)
                ->nullable()
                ->comment('Max discount, which is null on static discount type');

            $table->timestamp('created_at');
            $table->unsignedInteger('created_by')
                ->comment('Record agent_id in a real system on create operation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_code_histories');
    }
};
