<?php

namespace App\Http\Controllers\v1;

use App\Enum\SystemMessage;
use App\Http\Controllers\Controller;
use App\Http\Resources\DiscountArchiveCollection;
use App\Http\Resources\DiscountArchiveResource;
use App\Models\DiscountCodeArchive;
use Illuminate\Support\Facades\Response;

class DiscountArchiveController extends Controller
{
    public function index()
    {
        $filters = request([
            'discount_id',
            'status',
            'code',
            'currency_id',
            'type',
            'created_by',
        ]);

        $archives = DiscountCodeArchive::query()
            ->filter($filters)
            ->with('discountCode')
            ->orderByDesc('id')
            ->paginate(10);

        return Response::success(
            SystemMessage::SUCCESS, 'Data fetched',
            new DiscountArchiveCollection($archives)
        );
    }

    public function show(DiscountCodeArchive $archive)
    {
        return Response::success(
            SystemMessage::SUCCESS, 'Data fetched',
            new DiscountArchiveResource($archive)
        );
    }
}
