<?php

namespace App\Services\v1;

use App\Enum\DiscountHistoryStatus;
use App\Enum\SystemMessage;
use App\Exceptions\Discount\CurrencyMismatchException;
use App\Exceptions\Discount\DiscountAlreadyUsedException;
use App\Exceptions\Discount\DiscountExpiredException;
use App\Exceptions\Discount\DiscountUnavailableException;
use App\Models\DiscountCode;
use App\Models\DiscountHistory;
use Illuminate\Support\Facades\DB;

class DiscountService
{
    public function redeem(int $order_id, int $user_id, int $currency_id, string $code): DiscountHistory
    {
        return DB::transaction(function() use ($order_id, $user_id, $currency_id, $code) {
            $discount = DiscountCode::lockForUpdate()
                ->where('code', $code)
                ->first();

            if ($discount->isNotActive())
                throw new DiscountUnavailableException('Discount is unavailable', SystemMessage::DISCOUNT_UNAVAILABLE->value);

            if ($discount->isExpired())
                throw new DiscountExpiredException('Discount has been expired', SystemMessage::DISCOUNT_EXPIRED->value);

            if ($discount->currency_id != $currency_id)
                throw new CurrencyMismatchException('Discount and order currencies are not same', SystemMessage::CURRENCY_MISMATCH->value);

            if ($discount->isUsed($order_id, $user_id))
                throw new DiscountAlreadyUsedException('Discount already used', SystemMessage::DISCOUNT_ALREADY_USED->value);

            $discount_history = $discount->histories()
                ->create([
                    'status' => DiscountHistoryStatus::SUCCESS,
                    'order_id' => $order_id,
                    'user_id' => $user_id,
                    'type' => $discount->type,
                    'amount' => $discount->amount,
                    'max_amount' => $discount->max_amount
                ]);

            $discount->update([
                'submitted_times' => ++$discount->submitted_times
            ]);

            return $discount_history;
        });
    }
}
