<?php

namespace App\Models;

use App\Enum\DiscountCodeStatus;
use App\Enum\DiscountType;
use App\Traits\CreatedAt;
use App\Traits\CreatedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountCodeArchive extends Model
{
    use HasFactory, CreatedAt, CreatedBy;

    public $timestamps = false;

    protected $casts = [
        'status' => DiscountCodeStatus::class,
        'discount_type' => DiscountType::class,
        'expire_date' => 'datetime',
        'created_at' => 'datetime'
    ];

    public function scopeFilter(Builder $query, $filters)
    {
        $query->when($filters['discount_id'] ?? false, fn($query, $discount_id) =>
            $query->where('discount_id', $discount_id)
        );

        $query->when($filters['status'] ?? false, fn($query, $status) =>
            $query->where('status', $status)
        );

        $query->when($filters['code'] ?? false, fn($query, $code) =>
            $query->where('code', $code)
        );

        $query->when($filters['currency_id'] ?? false, fn($query, $currency_id) =>
            $query->where('currency_id', $currency_id)
        );

        $query->when($filters['type'] ?? false, fn($query, $type) =>
            $query->where('type', $type)
        );

        $query->when($filters['created_by'] ?? false, fn($query, $created_by) =>
            $query->where('created_by', $created_by)
        );
    }

    public function discountCode()
    {
        return $this->belongsTo(DiscountCode::class, 'discount_id');
    }
}
