<?php

namespace App\Models;

use App\Enum\DiscountCodeStatus;
use App\Traits\CreatedBy;
use App\Traits\DeletedBy;
use App\Traits\UpdatedBy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscountCode extends Model
{
    use HasFactory, CreatedBy, UpdatedBy, DeletedBy, SoftDeletes;

    protected $casts = [
        'status' => DiscountCodeStatus::class,
        'expire_date' => 'datetime'
    ];

    public function scopeFilter(Builder $query, array $filters)
    {
        $query->when($filters['status'] ?? false, fn($query, $status) =>
            $query->where('status', $status)
        );

        $query->when($filters['code'] ?? false, fn($query, $code) =>
            $query->where('code', $code)
        );

        $query->when($filters['currency_id'] ?? false, fn($query, $currency_id) =>
            $query->where('currency_id', $currency_id)
        );

        $query->when($filters['type'] ?? false, fn($query, $type) =>
            $query->where('type', $type)
        );

        $query->when($filters['created_by'] ?? false, fn($query, $created_by) =>
            $query->where('created_by', $created_by)
        );

        $query->when($filters['updated_by'] ?? false, fn($query, $updated_by) =>
            $query->where('updated_by', $updated_by)
        );
    }

    public function archives()
    {
        return $this->hasMany(DiscountCodeArchive::class, 'discount_id');
    }

    public function histories()
    {
        return $this->hasMany(DiscountHistory::class, 'discount_id');
    }

    public function isActive()
    {
        return $this->status == DiscountCodeStatus::ACTIVE;
    }

    public function isNotActive()
    {
        return !$this->isActive();
    }

    public function isExpired()
    {
        return $this->expire_date?->isPast() || (!is_null($this->max_times) && $this->max_times == $this->submitted_times);
    }

    public function isUsed(int $order_id, int $user_id)
    {
        $order_existence = $this->histories()
            ->where('order_id', $order_id)
            ->exists();

        $user_id_existence = $this->histories()
            ->where('user_id', $user_id)
            ->exists();

        return $order_existence || ($user_id_existence && $this->per_user_once);
    }
}
