<?php

namespace App\Enum;

enum SystemMessage: int
{
    case FAIL = 0;
    case SUCCESS = 1;
    case INTERNAL_ERROR = 10;
    case DATA_NOT_FOUND = 11;
    case BAD_DATA = 12;

    // Domain exception codes. starting from 100
    case DISCOUNT_UNAVAILABLE = 100;
    case DISCOUNT_EXPIRED = 101;
    case DISCOUNT_ALREADY_USED = 102;
    case CURRENCY_MISMATCH = 103;
}
