<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('status');

            $table->string('description')
                ->nullable()
                ->comment('Discount code description & title');

            $table->string('code', 30);

            $table->unsignedInteger('currency_id')
                ->comment('Describe max discount & submitted_discount currency unit');

            $table->unsignedInteger('type');

            $table->unsignedDecimal('amount', 19, 4)
                ->comment('Discount amount');

            $table->unsignedDecimal('max_amount', 19, 4)
                ->nullable()
                ->comment('Max discount, which is null on static discount type');

            $table->dateTime('expire_date')
                ->nullable();

            $table->unsignedInteger('submitted_times')
                ->default(0);

            $table->unsignedInteger('max_times')
                ->nullable();

            $table->boolean('per_user_once')
                ->default(false)
                ->comment('While true each user only can apply discount code once');

            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('created_by')
                ->comment('Record agent_id in a real system on create operation');

            $table->unsignedInteger('updated_by')
                ->nullable()
                ->comment('Record agent_id in a real system on update operation');

            $table->unsignedInteger('deleted_by')
                ->nullable()
                ->comment('Record agent_id in a real system on delete operation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_codes');
    }
};
