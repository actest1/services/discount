<?php

namespace App\Traits;

use Carbon\Carbon;

trait CreatedAt
{
    public static function bootCreatedAt()
    {
        static::creating(function ($model) {
            if ($model->isClean('created_at') && auth()->hasUser())
                $model->created_at = Carbon::now();
        });
    }

}
