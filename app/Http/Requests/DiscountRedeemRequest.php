<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscountRedeemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'order_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'currency_id' => ['required', 'integer'],
            'code' => ['required', 'alpha_dash', 'exists:discount_codes,code,deleted_at,NULL'],
        ];
    }
}
