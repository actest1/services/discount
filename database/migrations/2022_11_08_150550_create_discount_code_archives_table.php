<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_code_archives', function (Blueprint $table) {
            $table->id();

            $table->foreignId('discount_id')
                ->references('id')
                ->on('discount_codes')
                ->restrictOnDelete();

            $table->unsignedInteger('status');

            $table->string('description')
                ->nullable();

            $table->string('code', 30);

            $table->unsignedInteger('currency_id');

            $table->unsignedInteger('type');

            $table->unsignedDecimal('amount', 19, 4);

            $table->unsignedDecimal('max_amount', 19, 4)
                ->nullable();

            $table->dateTime('expire_date')
                ->nullable();

            $table->unsignedInteger('submitted_times');

            $table->unsignedInteger('max_times')
                ->nullable();

            $table->boolean('per_user_once');
            $table->timestamp('created_at');
            $table->unsignedInteger('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_code_logs');
    }
};
