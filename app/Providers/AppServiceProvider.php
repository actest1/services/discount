<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Response as HttpFoundation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Model::unguard();

        $this->registerResponseMacros();
    }

    private function registerResponseMacros() {
        Response::macro('success', fn($code, $message, $data = [], $http_status = HttpFoundation::HTTP_OK) =>
            Response::make([
                'code' => $code,
                'message' => $message,
                'data' => $data,
            ], $http_status)
        );

        Response::macro('error', fn($code, $message, $errors, $http_status = HttpFoundation::HTTP_BAD_REQUEST) =>
            Response::make([
                'code' => $code,
                'message' => $message,
                'errors' => $errors
            ], $http_status)
        );
    }
}
