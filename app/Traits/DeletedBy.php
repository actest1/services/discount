<?php

namespace App\Traits;

trait DeletedBy
{
    public static function bootDeletedBy()
    {
        static::deleting(function($model) {
            if ($model->isClean('deleted_by')) {
                $model->deleted_by = auth()->user()->id;
                $model->save();
            }
        });
    }
}
